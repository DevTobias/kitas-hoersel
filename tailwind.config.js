module.exports = {
  content: ['./pages/**/*.{js,ts,jsx,tsx}', './app/**/*.{js,ts,jsx,tsx}'],
  theme: {
    fontFamily: {
      poppins: ['Poppins', 'sans-serif'],
      inter: ['Inter', 'sans-serif'],
    },
    colors: {
      neutral: {
        900: '#212134',
        800: '#32324D',
        700: '#4A4A6A',
        600: '#666687',
        500: '#8E8EA9',
        400: '#A5A5BA',
        300: '#C0C0CF',
        200: '#DCDCE4',
        150: '#EAEAEF',
        100: '#F6F6F9',
        0: '#FFFFFF',
      },
      primary: {
        700: '#5A9CD9',
        600: '#7bb0e1',
        500: '#9cc4e8',
        200: '#bdd7f0',
        100: '#E4F5FF',
      },
      body: {
        700: '#7796B2',
      },
    },
    fontSize: {
      header1: ['60px', { lineHeight: '72px', letterSpacing: '0.01em' }],
      header1m: ['45px', { lineHeight: '56px', letterSpacing: '0.01em' }],

      header2: ['50px', { lineHeight: '64px', letterSpacing: '0.01em' }],
      header2m: ['31px', { lineHeight: '48px', letterSpacing: '0.01em' }],

      header3: ['40px', { lineHeight: '56px', letterSpacing: '0.01em' }],
      header3m: ['25px', { lineHeight: '40px', letterSpacing: '0.01em' }],

      header4: ['33px', { lineHeight: '40px', letterSpacing: '0.01em' }],
      header4m: ['21px', { lineHeight: '34px', letterSpacing: '0.01em' }],

      header5: ['24px', { lineHeight: '32px', letterSpacing: '0.01em' }],
      header5m: ['17px', { lineHeight: '32px', letterSpacing: '0.01em' }],

      body1: ['24px', { lineHeight: '38px', letterSpacing: '0.01em' }],
      body2: ['20px', { lineHeight: '34px', letterSpacing: '0.01em' }],
      body3: ['17px', { lineHeight: '28px', letterSpacing: '0.01em' }],
      body4: ['15px', { lineHeight: '24px', letterSpacing: '0.01em' }],
      body5: ['13px', { lineHeight: '22px', letterSpacing: '0.01em' }],
    },
    extend: {
      animation: {
        'bounce-right': 'bounce-right 1s ease-in-out infinite',
        pop: 'pop .6s ease',
        'fade-pop': 'fade-pop .4s ease-in-out',
      },
      keyframes: {
        'bounce-right': {
          '0%': { transform: 'translateX(0)' },
          '50%': { transform: 'translateX(7px)' },
        },
        pop: {
          '50%': { transform: 'scale(1.2)' },
        },
        'fade-pop': {
          '50%': { opacity: '0.2', transform: 'scale(1.3)' },
          '100%': { opacity: '0', transform: 'scale(1.7)' },
        },
      },
      height: {
        innerScreen: 'calc(var(--innerVh, 1vh) * 100)',
      },
      minHeight: {
        innerScreen: 'calc(var(--innerVh, 1vh) * 100)',
      },
    },
  },
  plugins: [],
};
