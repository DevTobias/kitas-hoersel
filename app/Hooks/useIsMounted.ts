import { useEffect, useState } from 'react';

/**
 * Hook to determine if the component already did mount or not.
 *
 * @returns True if the component is mounted, false instead.
 */
const useIsMounted = () => {
  const [mounted, setMounted] = useState(false);
  useEffect(() => setMounted(true), []);
  return mounted;
};

export default useIsMounted;
