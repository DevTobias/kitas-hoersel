import classNames from '@Utils/classNames';
import { FunctionComponent } from 'react';

import type { ContainerProps } from './Container.types';

/**
 * Container component with min screen height.
 */
const Container: FunctionComponent<ContainerProps> = ({
  children,
  className = '',
}) => {
  return (
    <div
      className={classNames(
        className,
        'bg-neutral-0 text-neutral-800 min-h-innerScreen flex flex-col mx-auto justify-center items-center space-y-5 container p-5'
      )}
    >
      {children}
    </div>
  );
};

export default Container;
