export type BottomNavProps = {
  className?: string;
};

export type LinkProps = {
  className?: string;
  href?: string;
  withDivider?: boolean;
};
