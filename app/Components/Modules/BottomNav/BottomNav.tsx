import { FunctionComponent } from 'react';
import Link from 'next/link';

import classNames from '@Utils/classNames';

import type { BottomNavProps, LinkProps } from './BottomNav.types';

interface BottomNavComposition {
  Link: FunctionComponent<LinkProps>;
}

/**
 * Generates a bottom navigation wrapper to place links.
 *
 * @param className  ('') The Styling which should be applied to the component.
 *
 * @example
 * <BottomNav>
 *  <BottomNav.Link href="/contact">Support</BottomNav.Link>
 * </BottomNav>
 */
const BottomNav: FunctionComponent<BottomNavProps> & BottomNavComposition = ({
  children,
  className = '',
}) => (
  <nav>
    <ul
      className={classNames(
        className,
        'text-neutral-700 h-fit body3 flex space-x-3 justify-center md:body2'
      )}
    >
      {children}
    </ul>
  </nav>
);

/**
 * Generates a link for the bottom nav component.
 *
 * @param className   ('')    The Styling which should be applied to the component.
 * @param href        ('')    The link which the user should get linked to after clicking.
 * @param withDivider (false) Whether a divider should be placed after the link or not.
 *
 * @example <BottomNav.Link href="/contact">Support</BottomNav.Link>
 */
const BottomNavLink: FunctionComponent<LinkProps> = ({
  children,
  className = '',
  href = '',
  withDivider = false,
}) => {
  return (
    <>
      <li>
        <Link href={href}>
          <a
            className={classNames(
              className,
              'after-underline after:bg-primary-500'
            )}
          >
            {children}
          </a>
        </Link>
      </li>

      {withDivider && (
        <li className="text-neutral-400" aria-hidden>
          |
        </li>
      )}
    </>
  );
};

BottomNav.Link = BottomNavLink;

export default BottomNav;
