import ArrowRightIcon from '@heroicons/react/solid/esm/ArrowRightIcon';
import Link from 'next/link';
import { FunctionComponent } from 'react';
import Image from 'next/image';

import Container from '@Layouts/Container';
import BottomNav from '@Modules/BottomNav';

const NotFoundTemplate: FunctionComponent = () => {
  return (
    <Container className="h-innerScreen md:relative justify-between">
      <main className="flex items-center h-[95%]">
        <div className="w-full space-y-12 text-center flex flex-col items-center md:items-start md:text-left md:max-w-xl">
          <span className="text-primary-700 body2 font-semibold uppercase md:header5">
            404 Fehler
          </span>
          <section className="space-y-4">
            <h1 className="text-neutral-800 font-bold header1m md:header1">
              Seite nicht gefunden.
            </h1>
            <p className="text-neutral-700 body3 md:body2">
              Es tut uns sehr leid für die Unannehmlichkeiten, aber wir konnten
              die von Ihnen gesuchte Seite nicht finden.
            </p>
          </section>
          <Link href="/">
            <a className="text-primary-700 body3 md:body2 font-medium flex space-x-3 w-fit items-center group">
              <span>Zurück zur Startseite</span>
              <ArrowRightIcon className="h-5 w-5 group-hover:animate-bounce-right" />
            </a>
          </Link>
        </div>
        <span aria-hidden className="hidden absolute md:block md:static">
          <Image
            src="/images/error_dog.svg"
            alt="dog holding a 404 error in his paws"
            width={1000}
            height={700}
          />
        </span>
      </main>

      <footer>
        <BottomNav>
          <BottomNav.Link href="/contact" withDivider>
            Kontakt
          </BottomNav.Link>
          <BottomNav.Link href="api/health-check" withDivider>
            Status
          </BottomNav.Link>
          <BottomNav.Link href="">Twitter</BottomNav.Link>
        </BottomNav>
      </footer>
    </Container>
  );
};

export default NotFoundTemplate;
