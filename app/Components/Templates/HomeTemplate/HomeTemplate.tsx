import SwitchHorizontalIcon from '@heroicons/react/solid/esm/SwitchHorizontalIcon';
import { FunctionComponent, useState } from 'react';
import Image from 'next/image';
import Link from 'next/link';
import { Parallax } from 'react-scroll-parallax';

import { Cloud, Sun, Underline, ArrowDown } from '@Elements/VectorGraphic';
import useWindowSize from '@Hooks/useWindowSize';

import styles from './HomeTemplate.module.scss';
import useIsMounted from '@Hooks/useIsMounted';
import classNames from '@Utils/classNames';

const Hero = () => {
  const [loadedLeft, setLoadedLeft] = useState(false);
  const [loadedRight, setLoadedRight] = useState(false);

  return (
    <div className="grid grid-cols-12 grid-rows-6 h-[calc(100vw*0.24+10*(100vw*0.24)/6)] lg:h-[calc(100vw*0.24+(100vw*0.24)/1.3)]">
      <div className="row-start-1 row-end-5 lg:row-end-4 col-start-1 col-end-13 w-full h-full bg-primary-100" />

      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 1440 320"
        className="row-start-5 lg:row-start-4 row-end-7 col-start-1 col-end-13"
      >
        <path
          fill="#E4F5FF"
          fillOpacity="1"
          d="M0,288L34.3,277.3C68.6,267,137,245,206,250.7C274.3,256,343,288,411,298.7C480,309,549,299,617,277.3C685.7,256,754,224,823,192C891.4,160,960,128,1029,96C1097.1,64,1166,32,1234,42.7C1302.9,53,1371,107,1406,133.3L1440,160L1440,0L1405.7,0C1371.4,0,1303,0,1234,0C1165.7,0,1097,0,1029,0C960,0,891,0,823,0C754.3,0,686,0,617,0C548.6,0,480,0,411,0C342.9,0,274,0,206,0C137.1,0,69,0,34,0L0,0Z"
        ></path>
      </svg>

      <div className="row-start-1 row-end-3 col-start-2 col-end-3">
        <Sun className="w-full h-full" />
      </div>

      <div className="row-start-4 row-end-5 col-start-4 lg:col-start-5 col-end-6 lg:col-end-7 flex items-start justify-center">
        <Underline className="w-full h-1/2" />
      </div>

      <div className="row-start-4 row-end-5 col-start-2 col-end-3">
        <Cloud className="w-1/2 h-1/2 text-neutral-0" />
      </div>

      <div className="row-start-1 row-end-2 col-start-11 col-end-12 flex items-end">
        <Cloud className="w-2/3 h-2/3 text-neutral-0" />
      </div>

      <div className="row-start-5 row-end-6 col-start-6 col-end-8 flex items-end animate-bounce">
        <ArrowDown className="w-full h-2/3 lg:h-full" />
      </div>

      <div className="row-start-4 row-end-7 col-start-8 lg:col-start-9 col-end-12 relative">
        <div className="absolute lg:-top-[30%] select-none pointer-events-none">
          <Parallax
            easing="easeOutQuad"
            translateY={['0', '-25%']}
            startScroll={0}
            endScroll={500}
            className={loadedRight && styles['slide-in-right']}
          >
            <Image
              src="/images/children.png"
              alt="two children hugging each other"
              width={1000}
              height={1000}
              onLoad={() => setLoadedRight(true)}
            />
          </Parallax>
        </div>
      </div>

      <div className="row-start-3 row-end-7 col-start-1 col-end-6 relative">
        <div className="absolute top-[45%] lg:top-[10%] select-none pointer-events-none">
          <Parallax
            easing="easeOutQuad"
            translateY={['0', '-30%']}
            startScroll={0}
            endScroll={500}
            className={loadedLeft && styles['slide-in-left']}
          >
            <Image
              src="/images/children_2.png"
              alt="two children swinging"
              width={1000}
              height={1000}
              onLoad={() => setLoadedLeft(true)}
            />
          </Parallax>
        </div>
      </div>

      <div className="row-start-2 row-end-3 col-start-4 col-end-10 flex items-start lg:items-end justify-center">
        <h3 className="text-primary-500 text-[3vw] leading-[3vw] lg:text-body1 z-10 text-center mt-[7%] lg:m-0">
          Willkommen bei den
        </h3>
      </div>

      <div className="row-start-2 lg:row-start-3 row-end-4 lg:row-end-4 col-start-2 lg:col-start-4 col-end-12 lg:col-end-10 flex items-end justify-center">
        <h1 className="text-primary-700 text-[6vw] leading-[7vw] lg:text-[3vw] lg:leading-[3.5vw] font-semibold text-center">
          Kindertagesstädten der <br /> Gemeinde Hörsel
        </h1>
      </div>
    </div>
  );
};

const IntroSection = () => {
  const { width } = useWindowSize(1000);

  return (
    <section className="text-body-700 text-body4 space-y-4 md:text-body2 md:grid md:grid-cols-2 md:gap-6 md:justify-center md:items-center">
      <div className="space-y-4 lg:w-[75%] md:col-start-2 md:col-end-3 md:row-start-1 md:relative">
        <Parallax
          easing="easeOutQuad"
          translateX={[200, 0]}
          className="space-y-4"
          startScroll={width >= 1024 ? 200 : 0}
          endScroll={width >= 1024 ? 900 : 200}
        >
          <h2 className="text-primary-700 text-header3m md:text-header2 font-medium">
            Hand in Hand
          </h2>
          <p>
            Die Kindergärten <em> Dreikäsehoch </em> Mechterstädt und
            <em> Kleine Strolche </em> Teutleben sind die zwei kommunalen
            Kindereinrichtungen der Gemeinde Hörsel.
          </p>
          <p>
            Wir arbeiten Hand in Hand und bilden ein Team mit einer gemeinsamen
            Leitung.
          </p>
        </Parallax>

        <Cloud className="hidden md:block md:absolute md:-right-16 md:-top-1/2 md:text-primary-100" />
      </div>

      <div className="select-none pointer-events-none max-w-[75%] md:max-w-lg mx-auto md:col-start-1 md:col-end-1 md:row-start-1 md:relative">
        <Parallax
          easing="easeOutQuad"
          translateX={[-200, 0]}
          startScroll={width >= 1024 ? 200 : 50}
          endScroll={width >= 1024 ? 900 : 250}
        >
          <Image
            src="/images/team_all.png"
            alt="the whole team"
            width={2020}
            height={1572}
          />
        </Parallax>

        <Cloud className="hidden md:block md:absolute md:-left-20 md:-bottom-20 md:text-primary-100" />
      </div>
    </section>
  );
};

const ChooseSection = () => {
  const [loadedLeft, setLoadedLeft] = useState(false);
  const [loadedRight, setLoadedRight] = useState(false);

  return (
    <section className="text-body-700 text-body4 lg:text-body2 space-y-7 lg:text-center lg:flexable lg:items-center">
      <div className="space-y-2">
        <h2 className="text-primary-700 text-header3m lg:text-header2 font-medium">
          Besuche unsere Einrichtungen
        </h2>
        <p className="md:max-w-4xl">
          Hier kannst du dich genauer über die einzelnen Kindereinrichtungen der
          Gemeinde Hörsel informieren.
        </p>
      </div>

      <div className="bg-primary-700 h-36 md:h-64 overflow-hidden w-full rounded-md grid grid-cols-2 relative">
        <Link href="/teutleben">
          <a className="relative h-full w-full hover:scale-110 transition-all cursor-pointer">
            <div className="absolute left-1/2 -translate-x-1/2 top-1/2 -translate-y-1/2 z-20 text-neutral-0 text-center font-medium text-body4 md:text-header2m">
              Kleine Strolche <br />
              Teutleben
            </div>
            <div
              className={classNames(
                loadedLeft && styles['opacity-in'],
                'absolute lg:-top-1/3 -left-14 skew-x-6 after:absolute after:top-0 after:left-0 after:w-full after:h-full after:bg-[#FFA9D8]'
              )}
            >
              <Image
                src="/images/kita_te.jpg"
                alt="kindergarten of Teutleben"
                width={4096}
                height={3072}
                onLoad={() => setLoadedLeft(true)}
              />
            </div>
          </a>
        </Link>

        <Link href="/mechterstädt">
          <a className="relative h-full w-full hover:scale-110 transition-all cursor-pointer">
            <div className="absolute left-1/2 -translate-x-1/2 top-1/2 -translate-y-1/2 z-20 text-neutral-0 text-center font-medium text-body4 md:text-header2m">
              Dreikäsehoch <br />
              Mechterstädt
            </div>
            <div
              className={classNames(
                loadedRight && styles['opacity-in'],
                'absolute lg:-top-1/3 -right-14 left-2 skew-x-6 after:absolute after:top-0 after:left-0 after:w-full after:h-full after:bg-[#5A9CD9]'
              )}
            >
              <Image
                src="/images/kita_me.jpg"
                alt="kindergarten of Mechterstädt"
                width={4096}
                height={3072}
                onLoad={() => setLoadedRight(true)}
              />
            </div>
          </a>
        </Link>

        <div className="w-10 h-10 md:w-20 md:h-20 rounded-lg md:rounded-2xl bg-primary-700 absolute left-1/2 -translate-x-1/2 top-1/2 -translate-y-1/2 flex justify-center items-center">
          <SwitchHorizontalIcon className="w-6 h-6 md:w-16 md:h-16 text-neutral-0" />
        </div>
      </div>
    </section>
  );
};

const BlogSection = () => {
  return (
    <section className="text-body-700 text-body4 lg:text-body2 space-y-4 lg:text-center lg:flexable lg:items-center">
      <h2 className="text-primary-700 text-header3m lg:text-header2 font-medium">
        Aktuelles von uns
      </h2>
      <p className="lg:max-w-2xl">
        Hier findest du aktuelle Blockbeiträge von unseren Kindergärten, um
        immer auf dem Laufenden zu bleiben!
      </p>
    </section>
  );
};

const HomeTemplate: FunctionComponent = () => {
  const isMounted = useIsMounted();

  return (
    <main className="space-y-[15%] pb-10 lg:pb-[10%]">
      <Hero />

      <div className="container flex flex-row justify-center items-center mx-auto px-5">
        <div className={classNames(!isMounted && 'hidden', 'space-y-[15%]')}>
          <IntroSection />
          <ChooseSection />
          <BlogSection />
        </div>
      </div>
    </main>
  );
};

export default HomeTemplate;
