import { FunctionComponent } from 'react';

import type { VectorGraphicsProps } from './VectorGraphics.types';
import VectorGraphic from '../VectorGraphic';

/**
 * Generates an cloud.
 *
 * @param className ('')  The styling which should be applied to the component.
 * @param scale     (1)   The scale factor of the svg.
 *
 * @example <Cloud />
 */
export const Cloud: FunctionComponent<VectorGraphicsProps> = ({
  className = '',
  scale = 1,
}) => {
  return (
    <VectorGraphic
      title="cloud"
      width={76}
      height={41}
      scale={scale}
      className={className}
      ariaHidden
    >
      <path d="M12.784 15.305c-.001-.084-.004-.167-.004-.25C12.78 7.092 21.096.637 31.353.637c8.84 0 16.236 4.794 18.112 11.215 1.75-.76 3.764-1.195 5.912-1.195 6.701 0 12.133 4.217 12.133 9.419 0 .212-.012.423-.03.632 4.975 1.434 8.52 5.15 8.52 9.512 0 5.602-5.85 10.143-13.067 10.143h-46.6C7.313 40.363 0 34.686 0 27.683c0-6.056 5.47-11.12 12.784-12.378Z" />
    </VectorGraphic>
  );
};
