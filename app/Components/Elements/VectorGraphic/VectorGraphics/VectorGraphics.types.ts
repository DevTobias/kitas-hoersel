export type VectorGraphicsProps = {
  className?: string;
  scale?: number;
};
