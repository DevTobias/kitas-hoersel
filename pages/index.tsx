import type { NextPage } from 'next';

import Title from '@Helpers/Title';
import HomeTemplate from '@Templates/HomeTemplate';

const Index: NextPage = () => {
  return (
    <>
      <Title title="Kitas der Gemeinde Hörsel" />
      <HomeTemplate />
    </>
  );
};

export default Index;
