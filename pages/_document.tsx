import Document, { Html, Main, NextScript, Head } from 'next/document';

import getCsp from '@Utils/getCsp';
import FontLoader from '@Helpers/FontLoader';
import Metatags from '@Helpers/Metatags';
class CustomDocument extends Document {
  render() {
    return (
      <Html lang="de">
        <Head>
          <meta name="referrer" content="strict-origin" />
          <meta
            httpEquiv="Content-Security-Policy"
            content={getCsp(NextScript.getInlineScriptSource(this.props))}
          />

          <Metatags
            appName="Kitas der Gemeinde Hörsel"
            desc="Die Kindergärten „Dreikäsehoch“ Mechterstädt und „Kleine Strolche“ Teutleben sind die zwei kommunalen Kindereinrichtungen der Gemeinde Hörsel."
            domain="https://kitas-hoersel.vercel.app"
            themeColor="#5A9CD9"
          />

          <FontLoader />
        </Head>

        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default CustomDocument;
