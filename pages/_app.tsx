import type { AppProps } from 'next/app';
import { useEffect } from 'react';
import { ParallaxProvider } from 'react-scroll-parallax';

import innerVh from '@Utils/innerVh';

import '@Styles/base.scss';

function App({ Component, pageProps }: AppProps) {
  useEffect(() => innerVh(), []);
  return (
    <ParallaxProvider>
      <Component {...pageProps} />
    </ParallaxProvider>
  );
}

export default App;
