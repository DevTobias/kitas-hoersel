import Title from '@Helpers/Title';
import NotFoundTemplate from '@Templates/NotFoundTemplate';

const Custom404Page = () => {
  return (
    <>
      <Title title="Seite nicht gefunden" />
      <NotFoundTemplate />
    </>
  );
};

export default Custom404Page;
