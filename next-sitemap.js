module.exports = {
  siteUrl: process.env.SITE_URL || 'https://kitas-hoersel.vercel.app',
  generateRobotsTxt: true,
};
